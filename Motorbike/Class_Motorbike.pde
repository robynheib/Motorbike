final color RED = color(255,0,0);
final color BLUE = color(0,0,255);

class motorbike
{
  float x = 5; //members
  float y;
  float speed=2;
  float size=30; 
  color colour;
  
  motorbike(float y, color col)//constructor
  {
    this.y = y;
    this.speed = (float)random(5.0);
    this.colour = col;
  }
  
  void render()
  {
    float wheelHeight = size/3;
    fill(colour);
    triangle(x,y,x+size,y,x+size/2,y - size/2); //built-in triangle routine
    drawWheel(x,y,wheelHeight);
    drawWheel(x+size,y,wheelHeight);
  }
  
  void drawWheel(float x, float y, float size)
  {
    float inner = size*2/3;
    fill(0); 
    ellipse(x,y,size,size);
    fill(255);
    ellipse(x,y,inner,inner);
  }
  
  void move() 
  {
    speed = (float)random(5.0); //a random step [0..5]
    x=x+speed;
  }
  
  void update()
  {
    render();
    move();
  }
  
  boolean finished()
  {
    return x > (width-10); //screen width
  }
  
}