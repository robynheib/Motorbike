motorbike bike1, bike2;
float initialPlayerspeed = 5;
float playerspeed = initialPlayerspeed;
int gameMode = 0;
int score = 0;

void setup()
{
  size(700,200);
  bike1 = new motorbike(50, RED);
  bike2 = new motorbike(100, BLUE);
}

void draw()
{
  if (gameMode == 0)
  {
    background(125);
    textSize(40);
    text("press space to begin", width/4, height/2);
  }
  else if (gameMode == 1)
  {
    textSize(20);
    text(score, width/2, 20);
    if (bike1.finished() == true || bike2.finished() == true)
    {
      gameMode = 2;
      if (bike1.finished() == true)
      {
       score = score + 1; 
       background(125);
       textSize(20);
       text(score, width/2, 20);
      }
    }
    else
    {
      background(125);
      bike2.update(); 
      bike1.render();
    }
  }
  else if (gameMode == 2)
  {
    if (bike1.finished() == true)
      {
        fill(bike1.colour);
        textSize(40);
        text("YOU wins. Press space to retry or escape to quit.", 5, height/2);
      }
      else
      {
        fill(bike2.colour);
        textSize(40);
        text("Blue wins. \n Press space to retry or escape to quit.", 5, height/2);
      }
  }
}

void keyPressed()
{
  if(key == CODED)
  {
    if(keyCode == RIGHT && (bike1.finished() != true || bike2.finished() != true ) && gameMode == 1)
    {
      bike1.x = bike1.x + playerspeed;
      playerspeed = playerspeed + 1;
    }
    else
    {
      playerspeed = playerspeed -2;
    }
  }
  if(key == ' ' &&  (gameMode == 0 || gameMode == 2))
    {
      gameMode = 1;
      bike1.x = 1;
      bike2.x = 1;
      playerspeed = initialPlayerspeed;
    }
  if((key == 9) &&  gameMode != 0)
    {
      fill(0);
      score = 0;
      gameMode = 0;
    }
}